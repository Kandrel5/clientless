﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WARPBOT
{
    internal class Template
    {
        internal static string Render(string template, HashSet<TemplateEntry> entries)
        {
            string vals = "";
            foreach (TemplateEntry te in entries)
            {
                vals += $"<a href='chatcmd:///tell "+te.BotName+" !warp'>"+te.Desc+"</a><br>";
            }
            return template.Replace("[$ENTRIES]",vals);
        }
    }
}

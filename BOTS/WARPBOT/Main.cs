﻿using AOSharp.Clientless;
using AOSharp.Clientless.Chat;
using AOSharp.Clientless.Logging;
using AOSharp.Common.GameData;
using SmokeLounge.AOtomation.Messaging.Messages.ChatMessages;
using SmokeLounge.AOtomation.Messaging.Messages.N3Messages;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace WARPBOT
{
    public class Main : ClientlessPluginEntry
    {

        

        public enum BOTMODE
        {
            PUBLIC,
            RESTRICTED
        }

        public enum BOTSTATE
        {
            INITIALIZING,
            READY,
            BUSY,
            NEEDNANO,
            NANO
        }

        int OldCurrentNano = 0;
        string BotName = "";
        bool initialize = true;
        
        public string FILEMODE;
        public string FILEOWNERS;
        public string FILERESTRICTED;

        public string DIRECTORYHELP;
        public string FILEOWNERCOMMANDS;
        public string FILEUSERCOMMANDS;
        public string FILESCRIPTENTRIES;


        string FILESCRIPTTEMPLATE;

        string template = "";


        BOTSTATE botstate = BOTSTATE.INITIALIZING;
        BOTMODE botmode = BOTMODE.PUBLIC;

        HashSet<string> Owners = new HashSet<string>();
        HashSet<string> Restricted = new HashSet<string>();
        HashSet<TemplateEntry> Entries = new HashSet<TemplateEntry>();

        DateTime dtkick = new DateTime(0);
        DateTime dtreadnanovalue = new DateTime(0);
        
        DateTime dtInit = new DateTime(0);

        BufferedOutput bufferedOutput = new BufferedOutput(1000);

        public override void Init(string pluginDir)
        {

            BotName = Client.CharacterName.ToUpper();
            Logger.Information("WARPBOT::Init");
            FILESCRIPTTEMPLATE = Path.Combine(Path.Combine(pluginDir, "ScriptTemplate"), "Template.txt");
            FILEMODE = Path.Combine(Path.Combine(Path.Combine(pluginDir, "Config"), "MODE"), BotName + ".txt");
            FILEOWNERS = Path.Combine(Path.Combine(Path.Combine(pluginDir, "Config"), "OWNERS"), BotName + ".txt");
            FILERESTRICTED = Path.Combine(Path.Combine(Path.Combine(pluginDir, "Config"), "RESTRICTED"), BotName + ".txt");

            FILESCRIPTENTRIES = Path.Combine(Path.Combine(Path.Combine(pluginDir, "Config"), "SCRIPTENTRIES"), BotName + ".txt");

            try
            {
                foreach (string line in File.ReadAllLines(FILESCRIPTENTRIES))
                {

                    int splitpos = line.IndexOf(' ');
                    string bname = line.Substring(0, splitpos).Trim();
                    string href = line.Substring(splitpos).Trim();
                    Entries.Add(new TemplateEntry(bname, href));
                }

                template = Template.Render(File.ReadAllText(FILESCRIPTTEMPLATE), Entries);
            }
            catch { }

            DIRECTORYHELP = Path.Combine(pluginDir, "HELP");
            FILEOWNERCOMMANDS = Path.Combine(DIRECTORYHELP, "OwnerCommands.txt");
            FILEUSERCOMMANDS = Path.Combine(DIRECTORYHELP, "UserCommands.txt");

            dtInit = DateTime.Now;

            

            try
            {
                if (File.ReadAllText(FILEMODE).Replace(Environment.NewLine, "").ToUpper() == "RESTRICTED")
                {
                    botmode = BOTMODE.RESTRICTED;
                }
            }
            catch
            {
                //Defaults to public
            }


            try
            {
                foreach (string owner in File.ReadAllLines(FILEOWNERS))
                {
                    if (owner.Trim() == "") continue;
                    Owners.Add(owner.ToUpper().Trim());
                }
            }
            catch
            {
                //defaults no owners
            }

            try
            {
                foreach (string user in File.ReadAllLines(FILERESTRICTED))
                {
                    if (user.Trim() == "") continue;
                    Restricted.Add(user.ToUpper().Trim());
                }
            }
            catch
            {
                //defaults no users
            }

            Client.OnUpdate += OnUpdate;
            Client.Chat.PrivateMessageReceived += (e, msg) => HandlePrivateMessage(msg);
            AOSharp.Clientless.Team.TeamMember += OnTeamMember;
            AOSharp.Clientless.Team.TeamMemberLeft += OnTeamMemberLeft;
        }


        private void OnTeamMemberLeft(object sender, TeamMemberLeftEventsArgs e)
        {

            if (Team.Members.Count < 2)
            {
                botstate = BOTSTATE.READY;
            }

        }



        private void OnTeamMember(object sender, TeamMemberEventsArgs e)
        {
            string user = "";
            TeamMember tm = Team.Members.Where(i => i.Identity == e.Identity).FirstOrDefault();

            if (tm != null)
                user = tm.Name.ToUpper();


            if (Team.Members.Count == 1)
            {
                //triggered for me
                return;
            }


            if (Team.Members.Count > 2)
            {
                Team.Kick(e.Identity);
                bufferedOutput.AddMessage(new QueuedPrivateMessage((uint)e.Identity.Instance, $"Bot is not ready"));
                return;
            }

            if (
                botmode == BOTMODE.PUBLIC
                ||
                (botmode == BOTMODE.RESTRICTED && isUser(user))
                )
            {

                if (e.Identity != DynelManager.LocalPlayer.Identity)
                {
                    bufferedOutput.AddMessage(new QueuedPrivateMessage((uint)e.Identity.Instance, $"Warping...Don't move!!!"));
                    
                    DynelManager.LocalPlayer.Cast(e.Identity, 154914);

                    dtkick = DateTime.Now;
                    bufferedOutput.AddMessage(new QueuedPrivateMessage((uint)e.Identity.Instance, $"I'll leave team after 22 seconds from now"));
                }

                botstate = BOTSTATE.BUSY;
            }
            else
            {
                Team.Kick(e.Identity);
                bufferedOutput.AddMessage(new QueuedPrivateMessage((uint)e.Identity.Instance, $"Dont abuse me!"));
                return;
            }

        }

        private void OnUpdate(object _, double deltaTime)
        {
            if (!Client.InPlay) return;

            if (((DateTime.Now - dtInit).TotalSeconds) < 10)
            {
                //for cleanup offline user messages
                return;
            }

            bufferedOutput.Pulse();

            if (initialize)
            {
                AOSharp.Clientless.DynelManager.LocalPlayer.MovementComponent.ChangeMovement(MovementAction.LeaveSit);
                AOSharp.Clientless.DynelManager.LocalPlayer.Cast(AOSharp.Clientless.DynelManager.LocalPlayer, 223372);
                initialize = false;
                botstate = BOTSTATE.READY;
            }



            if (botstate == BOTSTATE.BUSY)
            {
                if ((DateTime.Now - dtkick).TotalSeconds > 22) // after 22 seconds bot quit team
                {
                    Team.LeaveTeam();
                    botstate = BOTSTATE.READY;
                }
            }

            if (botstate == BOTSTATE.READY)
                if (DynelManager.LocalPlayer.GetStat(Stat.CurrentNano) < 881) //BW nanocost
                {
                    botstate = BOTSTATE.NEEDNANO;
                    AOSharp.Clientless.DynelManager.LocalPlayer.MovementComponent.ChangeMovement(MovementAction.SwitchToSit);
                    return; // sit-stand transition, let app breath before falling the next state. 
                }

            if (botstate == BOTSTATE.NEEDNANO)
            {
                if (DynelManager.LocalPlayer.GetStat(Stat.CurrentNano) >= 881)
                {
                    botstate = BOTSTATE.READY;
                    AOSharp.Clientless.DynelManager.LocalPlayer.MovementComponent.ChangeMovement(MovementAction.LeaveSit);
                    return; // sit-stand transition, let app breath before falling the next state. 
                }
                else
                {
                    if (!AOSharp.Clientless.DynelManager.LocalPlayer.Cooldowns.ContainsKey(Stat.Treatment))
                    {

                        if (Inventory.Items.Find(291083, out Item item)) //Health and Nano Recharger
                        {
                            botstate = BOTSTATE.NANO;
                            item.Use();
                            OldCurrentNano = DynelManager.LocalPlayer.GetStat(Stat.CurrentNano);
                            dtreadnanovalue = DateTime.Now;
                        }
                    }
                }
            }


            if (botstate == BOTSTATE.NANO)
            {
                if ((DateTime.Now - dtreadnanovalue).TotalSeconds > 5) // 5 seconds nano readings
                {
                    if (DynelManager.LocalPlayer.GetStat(Stat.CurrentNano) == OldCurrentNano
                        &&
                        DynelManager.LocalPlayer.GetStat(Stat.CurrentNano) >= 881
                        )
                    {
                        AOSharp.Clientless.DynelManager.LocalPlayer.MovementComponent.ChangeMovement(MovementAction.LeaveSit);
                        botstate = BOTSTATE.READY;
                        return; // sit-stand transition, let app breath before falling the next state. 
                    }

                    OldCurrentNano = DynelManager.LocalPlayer.GetStat(Stat.CurrentNano);
                    dtreadnanovalue = DateTime.Now;
                }
            }

        }

        private void HandlePrivateMessage(PrivateMessage msg)
        {
            //purge offline messages for cleanup
            if (botstate == BOTSTATE.INITIALIZING) return;

            if (!msg.Message.StartsWith("!"))
            {
                bufferedOutput.AddMessage(new QueuedPrivateMessage(msg.SenderId, $"Type !help"));
                return;
            }
            if (botstate == BOTSTATE.INITIALIZING) return;

            
            string[] commandParts = msg.Message.Split(' ');

            switch (commandParts[0].Remove(0, 1).ToLower())
            {




                ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                case "setpublic":

                    if (!isOwner(msg.SenderName.ToUpper()))
                    {
                        bufferedOutput.AddMessage(new QueuedPrivateMessage(msg.SenderId, $"You dont own me"));
                        return;
                    }

                    if (botmode != BOTMODE.PUBLIC)
                    {
                        botmode = BOTMODE.PUBLIC;
                        File.WriteAllText(FILEMODE, "PUBLIC");
                        bufferedOutput.AddMessage(new QueuedPrivateMessage(msg.SenderId, $"Bot is now in public mode"));
                    }
                    else
                    {
                        bufferedOutput.AddMessage(new QueuedPrivateMessage(msg.SenderId, $"Bot already public mode"));
                    }
                    break;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                case "setrestricted":

                    if (!isOwner(msg.SenderName.ToUpper()))
                    {
                        bufferedOutput.AddMessage(new QueuedPrivateMessage(msg.SenderId, $"You dont own me"));
                        return;
                    }



                    if (botmode != BOTMODE.RESTRICTED)
                    {
                        botmode = BOTMODE.RESTRICTED;
                        File.WriteAllText(FILEMODE, "RESTRICTED");
                        bufferedOutput.AddMessage(new QueuedPrivateMessage(msg.SenderId, $"Bot is now in restricted mode"));
                    }
                    else
                    {
                        bufferedOutput.AddMessage(new QueuedPrivateMessage(msg.SenderId, $"Bot already restricted mode"));
                    }

                    break;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

                case "listowners":
                    if (!isOwner(msg.SenderName.ToUpper()))
                    {
                        bufferedOutput.AddMessage(new QueuedPrivateMessage(msg.SenderId, $"You dont own me"));
                        return;
                    }

                    bufferedOutput.AddMessage(new QueuedPrivateMessage(msg.SenderId, $"******************************"));
                    bufferedOutput.AddMessage(new QueuedPrivateMessage(msg.SenderId, $"Owners list"));

                    string lowners = "";
                    foreach (string owner in Owners)
                        lowners += owner + ",";
                    
                    if (lowners.EndsWith(","))
                        lowners = lowners.Substring(0, lowners.Length - 1);
                    
                    bufferedOutput.AddMessage(new QueuedPrivateMessage(msg.SenderId, lowners));
                    bufferedOutput.AddMessage(new QueuedPrivateMessage(msg.SenderId, $"******************************"));

                    break;
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                case "listusers":

                    if (!isOwner(msg.SenderName.ToUpper()))
                    {
                        bufferedOutput.AddMessage(new QueuedPrivateMessage(msg.SenderId, $"You dont own me"));
                        return;
                    }

                    bufferedOutput.AddMessage(new QueuedPrivateMessage(msg.SenderId, $"******************************"));
                    bufferedOutput.AddMessage(new QueuedPrivateMessage(msg.SenderId, $"Users  list"));

                    string lusers = "";
                    foreach (string user in Restricted)
                        lusers += user + ",";

                    if (lusers.EndsWith(","))
                        lusers = lusers.Substring(0, lusers.Length - 1);

                    bufferedOutput.AddMessage(new QueuedPrivateMessage(msg.SenderId, lusers));

                    
                    bufferedOutput.AddMessage(new QueuedPrivateMessage(msg.SenderId, $"******************************"));
                    break;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////                
                case "addowner":

                    if (!isOwner(msg.SenderName.ToUpper()))
                    {
                        bufferedOutput.AddMessage(new QueuedPrivateMessage(msg.SenderId, $"You dont own me"));
                        return;
                    }

                    try
                    {
                        string newowner = commandParts[1].Trim().ToUpper();
                        if (!Owners.Contains(newowner))
                        {
                            Owners.Add(newowner);
                            File.WriteAllLines(FILEOWNERS, Owners);
                            bufferedOutput.AddMessage(new QueuedPrivateMessage(msg.SenderId, $"Owner added"));
                        }
                        else
                        {
                            bufferedOutput.AddMessage(new QueuedPrivateMessage(msg.SenderId, $"That owner already exists"));
                        }

                    }
                    catch
                    {
                        bufferedOutput.AddMessage(new QueuedPrivateMessage(msg.SenderId, $"Unable to addowner."));
                    }
                    break;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                case "remowner":


                    if (!isOwner(msg.SenderName.ToUpper()))
                    {
                        bufferedOutput.AddMessage(new QueuedPrivateMessage(msg.SenderId, $"You dont own me"));
                        return;
                    }


                    try
                    {
                        string newowner = commandParts[1].Trim().ToUpper();
                        if (Owners.Contains(newowner))
                        {
                            Owners.Remove(newowner);
                            File.WriteAllLines(FILEOWNERS, Owners);
                            bufferedOutput.AddMessage(new QueuedPrivateMessage(msg.SenderId, $"Owner removed"));
                        }
                        else
                        {
                            bufferedOutput.AddMessage(new QueuedPrivateMessage(msg.SenderId, $"That owner doesnt exists"));
                        }

                    }
                    catch
                    {
                        bufferedOutput.AddMessage(new QueuedPrivateMessage(msg.SenderId, $"Unable to remowner."));
                    }
                    break;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

                case "adduser":
                    if (!isOwner(msg.SenderName.ToUpper()))
                    {
                        bufferedOutput.AddMessage(new QueuedPrivateMessage(msg.SenderId, $"You dont own me"));
                        return;
                    }

                    try
                    {
                        string newuser = commandParts[1].Trim().ToUpper();
                        if (!Restricted.Contains(newuser))
                        {
                            Restricted.Add(newuser);
                            File.WriteAllLines(FILERESTRICTED, Restricted);
                            bufferedOutput.AddMessage(new QueuedPrivateMessage(msg.SenderId, $"User added"));
                        }
                        else
                        {
                            bufferedOutput.AddMessage(new QueuedPrivateMessage(msg.SenderId, $"That user already exists"));
                        }

                    }
                    catch
                    {
                        bufferedOutput.AddMessage(new QueuedPrivateMessage(msg.SenderId, $"Unable to adduser."));
                    }

                    break;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                case "remuser":

                    if (!isOwner(msg.SenderName.ToUpper()))
                    {
                        bufferedOutput.AddMessage(new QueuedPrivateMessage(msg.SenderId, $"You dont own me"));
                        return;
                    }


                    try
                    {
                        string newuser = commandParts[1].Trim().ToUpper();
                        if (Restricted.Contains(newuser))
                        {
                            Restricted.Remove(newuser);
                            File.WriteAllLines(FILERESTRICTED, Restricted);
                            bufferedOutput.AddMessage(new QueuedPrivateMessage(msg.SenderId, $"User removed"));
                        }
                        else
                        {
                            bufferedOutput.AddMessage(new QueuedPrivateMessage(msg.SenderId, $"That user doesnt exists"));
                        }

                    }
                    catch
                    {
                        bufferedOutput.AddMessage(new QueuedPrivateMessage(msg.SenderId, $"Unable to remuser."));
                    }
                    break;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

                case "status":

                    if (botstate == BOTSTATE.INITIALIZING)
                    {
                        bufferedOutput.AddMessage(new QueuedPrivateMessage(msg.SenderId, $"Still initializing..."));
                        return;
                    }

                    Vector3 botpos = DynelManager.LocalPlayer.Transform.Position;
                    bufferedOutput.AddMessage(new QueuedPrivateMessage(msg.SenderId, $"*****************************"));
                    bufferedOutput.AddMessage(new QueuedPrivateMessage(msg.SenderId, $"State: " + botstate.ToString()));
                    bufferedOutput.AddMessage(new QueuedPrivateMessage(msg.SenderId, $"Current nano(need 881): " + DynelManager.LocalPlayer.GetStat(Stat.CurrentNano).ToString()));
                    bufferedOutput.AddMessage(new QueuedPrivateMessage(msg.SenderId, $"Mode: " + botmode.ToString()));
                    bufferedOutput.AddMessage(new QueuedPrivateMessage(msg.SenderId, $"Playfield: " + Playfield.Name));
                    bufferedOutput.AddMessage(new QueuedPrivateMessage(msg.SenderId, $"Map: /waypoint " + botpos.X.ToString() + " " + botpos.Z.ToString() + " " + Playfield.ModelId.ToString()));
                    bufferedOutput.AddMessage(new QueuedPrivateMessage(msg.SenderId, $"*****************************"));
                    break;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                
                case "warp":

                    if (botmode == BOTMODE.RESTRICTED && !isUser(msg.SenderName))
                    {
                        bufferedOutput.AddMessage(new QueuedPrivateMessage(msg.SenderId, $"You cant use me"));
                        return;

                    }

                    if (botstate != BOTSTATE.READY)
                    {
                        bufferedOutput.AddMessage(new QueuedPrivateMessage(msg.SenderId, $"Bot is not ready"));
                        return;
                    }
                    
                    Team.Invite(new Identity((int)msg.SenderId));
                    //Client.Send(
                    //new CharacterActionMessage()
                    //{
                    //    Action = CharacterActionType.TeamRequest,
                    //    Target = new Identity((int)msg.SenderId),
                    //    Parameter1 = 1
                    //});
                    break;


                case "canwarp":
                    if (isUser(msg.SenderName.ToUpper()))
                        bufferedOutput.AddMessage(new QueuedPrivateMessage(msg.SenderId, $"[YES]"));
                    else
                        bufferedOutput.AddMessage(new QueuedPrivateMessage(msg.SenderId, $"[NO]"));
                    break;


                ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                case "locinfo":
                    Vector3 botposition = DynelManager.LocalPlayer.Transform.Position;
                    bufferedOutput.AddMessage(new QueuedPrivateMessage(msg.SenderId, $"[locinfo: " + botposition.X.ToString() + " " + botposition.Z.ToString() + " " + Playfield.ModelId.ToString()+ " " + Playfield.Name + "]" ));
                    break;

                ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

                case "ping":
                    bufferedOutput.AddMessage(new QueuedPrivateMessage(msg.SenderId, $"[pong]"));
                    break;

                ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


                case "addwarp":

                    if (!isOwner(msg.SenderName.ToUpper()))
                    {
                        bufferedOutput.AddMessage(new QueuedPrivateMessage(msg.SenderId, $"You dont own me"));
                        return;
                    }


                    try
                    {
                            string msgwithoutcommand = msg.Message.Substring(8).Trim();
                            int splitpos = msgwithoutcommand.IndexOf(' ');
                            string bname = msgwithoutcommand.Substring(0, splitpos).Trim();

                            if (Entries.Where(e => e.BotName.ToUpper() == bname.ToUpper()).FirstOrDefault()!=null)
                            {
                                bufferedOutput.AddMessage(new QueuedPrivateMessage(msg.SenderId, $"Warp entry already exists"));
                                return;
                            }
                        
                            string href = msgwithoutcommand.Substring(splitpos).Trim();
                            Entries.Add(new TemplateEntry(bname, href));

                            string filecontent = "";
                            foreach (TemplateEntry te in Entries)
                            {
                                filecontent += te.BotName + " " + te.Desc +Environment.NewLine;
                            }
                            File.WriteAllText(FILESCRIPTENTRIES, filecontent);
                            template = Template.Render(File.ReadAllText(FILESCRIPTTEMPLATE), Entries);
                            bufferedOutput.AddMessage(new QueuedPrivateMessage(msg.SenderId, $"Warp entry added"));
                    }
                    catch { }
                    break;

                ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


                case "remwarp":

                    if (!isOwner(msg.SenderName.ToUpper()))
                    {
                        bufferedOutput.AddMessage(new QueuedPrivateMessage(msg.SenderId, $"You dont own me"));
                        return;
                    }


                    try
                    {
                        string bname = commandParts[1].Trim();

                        TemplateEntry targetentry = Entries.Where(e => e.BotName.ToUpper() == bname.ToUpper()).FirstOrDefault();
                        if (targetentry == null)
                        {
                            bufferedOutput.AddMessage(new QueuedPrivateMessage(msg.SenderId, $"Warp entry does not exists"));
                            return;
                        }
                        Entries.Remove(targetentry);

                        string filecontent = "";
                        foreach (TemplateEntry te in Entries)
                        {
                            filecontent += te.BotName + " " + te.Desc;
                        }
                        File.WriteAllText(FILESCRIPTENTRIES, filecontent);
                        template = Template.Render(File.ReadAllText(FILESCRIPTTEMPLATE), Entries);
                        bufferedOutput.AddMessage(new QueuedPrivateMessage(msg.SenderId, $"Warp entry removerd"));
                    }
                    catch { }
                    break;


                ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                case "warps":
                    try
                    {
                        template = Template.Render(File.ReadAllText(FILESCRIPTTEMPLATE), Entries);
                    }
                    catch { }
                    bufferedOutput.AddMessage(new QueuedPrivateMessage(msg.SenderId, template));
                    break;


                case "help":
                    if (commandParts.Length == 1)
                    {
                        bufferedOutput.AddMessage(new QueuedPrivateMessage(msg.SenderId, $"*****************************"));
                        bufferedOutput.AddMessage(new QueuedPrivateMessage(msg.SenderId, $"COMMANDS LIST:"));

                        if (isOwner(msg.SenderName))
                        {
                            bufferedOutput.AddMessage(new QueuedPrivateMessage(msg.SenderId, $"[OWNERS]:"));

                            //string[] lines = File.ReadAllLines(FILEOWNERCOMMANDS);

                            foreach (string line in File.ReadAllLines(FILEOWNERCOMMANDS))
                            {
                                bufferedOutput.AddMessage(new QueuedPrivateMessage(msg.SenderId, line));
                            }
                        }

                        //if (isUser(msg.SenderName))
                        //{
                            bufferedOutput.AddMessage(new QueuedPrivateMessage(msg.SenderId, $"[USERS]:"));

                            //string[] lines = File.ReadAllLines(FILEUSERCOMMANDS);

                            foreach (string line in File.ReadAllLines(FILEUSERCOMMANDS))
                            {
                                bufferedOutput.AddMessage(new QueuedPrivateMessage(msg.SenderId, line));
                            }
                        //}

                        bufferedOutput.AddMessage(new QueuedPrivateMessage(msg.SenderId, $"*****************************"));
                        bufferedOutput.AddMessage(new QueuedPrivateMessage(msg.SenderId, $"For detailed help !help COMMAND"));
                        bufferedOutput.AddMessage(new QueuedPrivateMessage(msg.SenderId, $"Available warps:"+template));
                    }
                    else 
                    {

                        string cmd = commandParts[1].Trim().ToLower();

                        cmd = cmd.Replace("!","");

                        string hlpfile = Path.Combine(Path.Combine(DIRECTORYHELP,"DETAIL"), cmd + ".txt");

                        if (!File.Exists(hlpfile))
                        {
                            bufferedOutput.AddMessage(new QueuedPrivateMessage(msg.SenderId, $"Cant help with that"));
                            return;
                        }

                        string[] lines = File.ReadAllLines(hlpfile);
                        foreach (string line in lines)
                        {
                            bufferedOutput.AddMessage(new QueuedPrivateMessage(msg.SenderId, line));
                        }

                    }
                    
                    break;


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                default:
                    bufferedOutput.AddMessage(new QueuedPrivateMessage(msg.SenderId, $"Type !help"));
                    Logger.Warning($"Received unknown command: {msg.Message}");
                    break;
            }
        }

        private bool isOwner(string User)
        {
            return (Owners.Where(o => o == User.ToUpper()).FirstOrDefault() != null);
        }

        private bool isUser(string User)
        {
            return (Restricted.Where(o => o == User.ToUpper()).FirstOrDefault() != null);
        }
      
    }
    public class TemplateEntry
    {
        public string BotName;
        public string Desc;
        public TemplateEntry(string BotName,string Desc)
        {
            this.BotName = BotName;
            this.Desc = Desc;
        }
    }

}
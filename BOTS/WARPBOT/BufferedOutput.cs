﻿using AOSharp.Clientless;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WARPBOT
{
    internal class BufferedOutput
    {

        ConcurrentQueue<QueuedPrivateMessage> msgqueue = new ConcurrentQueue<QueuedPrivateMessage>();

        DateTime lastSentMsg = new DateTime(0);

        private int BUFFERED_OUTPUT_MILISECS;
        
        public BufferedOutput(int BUFFERED_OUTPUT_MILISECS)
        {
            this.BUFFERED_OUTPUT_MILISECS = BUFFERED_OUTPUT_MILISECS;
        }


        public void AddMessage(QueuedPrivateMessage qpm)
        {
            msgqueue.Enqueue(qpm);
        }


        public void Pulse()
        {
            if (msgqueue.Count > 0 && (DateTime.Now - lastSentMsg).TotalMilliseconds > BUFFERED_OUTPUT_MILISECS)
            {
                if (msgqueue.TryDequeue(out QueuedPrivateMessage qpm))
                {
                    DispatchMessages(qpm);
                    lastSentMsg = DateTime.Now;
                }
            }
        }


        public void BufferedFlush()
        {
            while (msgqueue.Count > 0)
            {
                if ((DateTime.Now - lastSentMsg).TotalMilliseconds > BUFFERED_OUTPUT_MILISECS)
                {
                    if (msgqueue.TryDequeue(out QueuedPrivateMessage qpm))
                    {
                        DispatchMessages(qpm);
                        lastSentMsg = DateTime.Now;
                    }
                }
            }
        }

        public void Flush()
        {
            while (msgqueue.Count > 0)
            {
                    if (msgqueue.TryDequeue(out QueuedPrivateMessage qpm))
                    {
                        DispatchMessages(qpm);
                    }
            }
        }




        private void DispatchMessages(QueuedPrivateMessage qpm)
        {
            Client.SendPrivateMessage(qpm.UserId, qpm.Msg);
        }

    }
    internal class QueuedPrivateMessage
    {
        public uint UserId;
        public string Msg;
        public QueuedPrivateMessage(uint UserId, string Msg)
        {
            this.UserId = UserId;
            this.Msg = Msg;
        }
    }
}

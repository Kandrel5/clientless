﻿
Imagine you have a bot named WARPBOT

FOLDER:
MODE
	=>Specifies if the bot is open for all, or just a restricted group.

	You should create a file on that folder named WARPBOT.TXT
	With just one line PUBLIC or RESTRICTED. The omission of this file, defaults to PUBLIC BEHAVIOR

	Content Example of MODE\WARBOT.TXT (Just 1 line)
	RESTRICTED

OWNERS:
	=>Specifies a list of players which can:
	=> SET THE BOT'S MODE PUBLIC OR RESTRICTED. !setpublic or !setrestricted
	=> Can manage bot owners and users
	=> SET THE RESTRICTED USER'S LIST ALLOWED TO USE BOT IN RESTRICTED MODE

	Content Example of OWNERS\WARBOT.TXT (1 line per owner)
	OWNER1
	OWNER2
	....
	OWNER N


RESTRICTED:
	=>Specifies a list of users which use the bot in a restriced mode:
	
	Content Example of RESTRICTED\WARBOT.TXT (1 line per user)
	USER1
	USER2
	....
	USER N

SCRIPTENTRIES:
	=>Specifies a list of warpable bots with their descriptions which are shown with !warps command
	
	Content Example of SCRIPTENTRIES\WARBOT.TXT (1 line per bot desc)
	BOTNAME DESCRITION

	Remarks: Bot name is considered until first space character, after that its considered a description
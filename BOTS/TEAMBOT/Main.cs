﻿using AOSharp.Clientless;
using AOSharp.Clientless.Chat;
using AOSharp.Clientless.Logging;
using AOSharp.Common.GameData;
using SmokeLounge.AOtomation.Messaging.GameData;
using SmokeLounge.AOtomation.Messaging.Messages.N3Messages;
using SmokeLounge.AOtomation.Messaging.Messages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TEAMBOT
{
    public class Main : ClientlessPluginEntry
    {
        public override void Init(string pluginDir)
        {

            Logger.Information("TEAMBOT::Init");

            //Client.OnUpdate += OnUpdate;
            Client.MessageReceived += OnMessageReceived;

            Client.Chat.PrivateMessageReceived += (e, msg) => HandlePrivateMessage(msg);
            Client.Chat.VicinityMessageReceived += (e, msg) => HandleVicinityMessage(msg);
            Client.Chat.GroupMessageReceived += (e, msg) => HandleGroupMessage(msg);

            AOSharp.Clientless.Team.TeamRequest += OnTeamRequested;
            AOSharp.Clientless.DynelManager.DynelSpawned += OnDynelSpawned;
            AOSharp.Clientless.DynelManager.DynelDespawned += OnDynelDespawned;
            AOSharp.Clientless.Playfield.TowerUpdate += OnTowerUpdate;


        }
        private void OnTeamRequested(object sender, AOSharp.Clientless.TeamRequestEventArgs e)
        {
            e.Accept();
        }


        private void HandleVicinityMessage(VicinityMsg msg)
        {
            Logger.Debug($"{msg.SenderName}: {msg.Message}");
        }

        private void HandlePrivateMessage(PrivateMessage msg)
        {
            if (!msg.Message.StartsWith("!"))
                return;

            string[] commandParts = msg.Message.Split(' ');


            switch (commandParts[0].Remove(0, 1).ToLower())
            {
                
                case "team":
                    
                    Client.Send(
                    new CharacterActionMessage()
                    {
                        Action = CharacterActionType.TeamRequest,
                        Target = new Identity((int)msg.SenderId),
                        Parameter1 = 1
                    });
                    break;

                case "teamcharid":

                    int charid;

                    if (commandParts.Length < 2 || !int.TryParse(commandParts[1], out charid))
                    {
                        Logger.Error($"Received cast command without valid params.");
                        return;
                    }

                    Client.Send(
                    new CharacterActionMessage()
                    {
                        Action = CharacterActionType.TeamRequest,
                        Target = new Identity(charid),
                        Parameter1 = 1
                    });
                    break;

                case "teamkickcharid":

                    int charid2;

                    if (commandParts.Length < 2 || !int.TryParse(commandParts[1], out charid2))
                    {
                        Logger.Error($"Received cast command without valid params.");
                        return;
                    }
                    Team.Kick(new Identity(charid2));
                    break;

                case "teamleave":
                    Team.LeaveTeam();
                    break;


                default:
                    Logger.Warning($"Received unknown command: {msg.Message}");
                    break;
            }
        }

        private void HandleGroupMessage(GroupMsg msg)
        {
            if (msg.ChannelId == AOSharp.Clientless.DynelManager.LocalPlayer.OrgId)
            {
                Logger.Information($"Received org message!");
            }
            else if (msg.ChannelId == AOSharp.Clientless.DynelManager.LocalPlayer.TeamId)
            {
                Logger.Information($"Received team message!");
            }

            Logger.Information($"[{msg.ChannelName}]: {msg.SenderName}: {msg.Message}");
        }

        private void OnDynelSpawned(object _, AOSharp.Clientless.Dynel dynel)
        {
            if (dynel is PlayerChar player)
            {
                Logger.Information($"Player Spawned: {player.Name} - {player.Transform.Position}");
            }
            else if (dynel is NpcChar npc)
            {
                Logger.Information($"NPC Spawned: {npc.Name} - {npc.Transform.Position}");
            }
        }

        private void OnDynelDespawned(object _, AOSharp.Clientless.Dynel dynel)
        {
            if (dynel is AOSharp.Clientless.SimpleChar simpleChar)
                Logger.Information($"Character Despawned: {simpleChar.Name} - {simpleChar.Transform.Position}");
        }

        private void OnMessageReceived(object _, Message msg)
        {
            if (msg.Header.PacketType == PacketType.PingMessage)
                Logger.Debug($"Plugin detected Ping message!");
        }

        private void OnTowerUpdate(object _, TowerUpdateEventArgs e)
        {
            Logger.Debug($"{e.Tower.Side} {e.Tower.Class} {e.Tower.TowerCharId} at {e.Tower.Position} - {e.UpdateType}");
        }

    }
}
